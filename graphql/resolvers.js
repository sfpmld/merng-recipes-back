const { generateToken } = require('../utils/jwt-utils');
const { verifyPassword  } = require('../utils/bcrypt-utils');

module.exports.resolvers = {
  Query: {
    getAllRecipes: async (root, args, { Recipe }) => {
      const allRecipes = await Recipe.find().sort({createdAt: 'desc'});
      return allRecipes
    },
    getRecipe: async (root, { _id }, { Recipe }) => {
      const recipe = await Recipe.findOne({_id: _id});
      return {
        ...recipe._doc,
        createdAt: recipe.createdAt.toISOString()
      }
    },
    searchRecipes: async (root, { data }, { Recipe }) => {
      if(data){
        const searchedRecipes = await Recipe.find({
          $text: { $search: data }
        },{
          score: { $meta: "textScore" }
        }).sort({
          score: { $meta: "textScore" }
        });
        return searchedRecipes;
      } else {
        const allRecipes = await Recipe.find().sort({ like: 'desc', createdAt: 'desc' });
        return allRecipes
      }
    },
    getCurrentUser: async (root, args, { currentUser, User }) => {
      //check if user authenticated
      if(!currentUser){
        return null;
      }

      // retrieving current user informations
      const user = await User.findOne({_id: currentUser._id}).populate({
        path: 'favorites',
        model: 'Recipe'
      });

      return {
        ...user._doc,
        createdAt: user._doc.createdAt.toISOString()
      };
    },
    getUserRecipes: async (root, { username }, { currentUser, Recipe }) => {
      //check if user authenticated
      if(!currentUser){
        return null;
      }

      //check recipes associated with the user logged in
      const userRecipes = await Recipe.find({ username }).sort({ createdAt: 'desc' })
      return userRecipes;
    },
  },
  Mutation: {
    // add recipe method
    addRecipe: async  (root, { data }, { Recipe }) => {
      const newRecipe = await new Recipe({
        name: data.name,
        description: data.description,
        category: data.category,
        instructions: data.instructions,
        username: data.username
      }).save();

      return newRecipe;
    },

    deleteUserRecipe: async (root, { _id }, { Recipe }) => {
      const deletedRecipe = await Recipe.findOneAndRemove({_id});

      return deletedRecipe;
    },

    signupUser: async (root, { data }, { User }) => {

      // check if user already exists
      const userExist = await User.findOne({email: data.email});
      if(userExist){
        throw new Error('User already created');
      }

      const newUser = await new User({
        username: data.username,
        password: data.password,
        email: data.email
      }).save();

      // creating token for user
      const token = generateToken({
        username: newUser.username,
        email: newUser.email
      });

      return {
        token
      }
    },

    signinUser: async (root, { data }, { User }) => {

      // check if user exists for this email
      const user = await User.findOne({email: data.email});
      if(!user){
        throw new Error('User not found.');
      }
      // check if password is correct
      const areSame = await verifyPassword(data.password, user.password);
      if(!areSame){
        throw new Error('Incorrect password.');
      }

      // password is correct -> generate token for user logged in
      const token = generateToken(user._doc);

      return { token };


    },
  }
}
