module.exports.typeDefs = `

  type Recipe {
    _id: ID
    name: String! @unique
    category: String!
    description: String!
    likes: Int
    instructions: String!
    username: String
    createdAt: String!
    updatedAt: String!
  }

  type User {
    _id: ID
    username: String!
    password: String!
    email: String!
    favorites: [Recipe!]
    createdAt: String!
    updatedAt: String!
  }

  type Query {
    getAllRecipes: [Recipe!]!
    getRecipe(_id: ID!): Recipe
    searchRecipes(data: String): [DataSearch]
    getCurrentUser: User
    getUserRecipes(username: String!): [Recipe]
  }

  type Mutation {
    addRecipe(data: AddRecipeInput!): Recipe!
    deleteUserRecipe(_id: ID!): Recipe!
    signupUser(data: SignupInput): Token!
    signinUser(data:SigninInput): Token!
  }

  type DataSearch {
    _id: ID
    name: String
    category: String
    description: String
    likes: Int
    instructions: String
    username: String
    createdAt: String
    updatedAt: String
  }

  type Token {
    token: String!
  }

  input SignupInput {
    username: String!
    email: String!
    password: String!
  }

  input SigninInput {
    email: String!
    password: String!
  }

  input AddRecipeInput {
    name: String!
    description: String!
    category: String!
    instructions: String!
    username: String
}

`
