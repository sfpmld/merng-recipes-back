const express = require('express');
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');
//graphql
const { graphiqlExpress, graphqlExpress } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');
// import models
const Recipe = require('./models/Recipe');
const User = require('./models/User');
//middlewares
const { getAuth } = require('./middlewares/getAuth');

// Loading env variables
const dotEnv = require('dotenv').config();
const port = process.env.PORT || '3000';
const MONGODB_PREFIX = process.env.MONGODB_PREFIX;
const DB_USERNAME = process.env.DB_USERNAME;
const DB_ROOT_PASSWORD = process.env.DB_ROOT_PASSWORD;
const DB_HOST = process.env.DB_HOST;
const DB_NAME = process.env.DB_NAME;
const MONGODB_URL_RULE = process.env.MONGODB_URL_RULE;

const MONGODB_URL = `${MONGODB_PREFIX}://${DB_USERNAME}:${DB_ROOT_PASSWORD}@${DB_HOST}/${DB_NAME}?${MONGODB_URL_RULE}`

const GRAPHQL_ENDPOINT = process.env.GRAPHQL_ENDPOINT || 'http://localhost:3000'

const app = express();



// CORS Error Handling
// app.use((req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');

//   // graphql setting
//   if(req.method === 'OPTIONS'){
//     return res.sendStatus(200);
//   }

//   next();
// });
// CORS Error Handling (alternative)
const corsOptions = {
  'origin': true,
  'credentials': true,
  'methods': "['OPTIONS','GET','HEAD','PUT','PATCH','POST','DELETE']",
  'allowHeaders': "['Content-Type', 'Authorization']",
  'optionsSuccessStatus': 200, /* some legacy browsers (IE11, various SmartTVs) choke on 204 */
  'preflightContinue': false
};

app.use(cors(corsOptions))

// check authorization middleware
app.use(getAuth);

// GraphQL Endpoint
// requiring typeDefs and resolvers
const { typeDefs } = require('./graphql/schema');
const { resolvers } = require('./graphql/resolvers');
// make schema executable
const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

// graphiql endpoint
app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql'
}));

// graphql endpoint
// connecting schemas with graphql
app.use('/graphql',
  bodyParser.json(),
  graphqlExpress(({ currentUser }) => ({
    schema,
    context: {
      User,
      Recipe,
      currentUser
    }
  })))

// main error handling middleware
app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;

  res.status(status).json({
    message: message,
    data: data
  });
});

// mongoose connection to mongoDB
mongoose.connect(MONGODB_URL, { useUnifiedTopology: true, useNewUrlParser: true })
  .then( result => {
      console.log('Connected to mongodb')
      app.listen(port);
      console.log('server started.');
  })
  .catch(err => console.log('Mongodb failed to connect!', err));


