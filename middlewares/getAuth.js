const { verifyToken } = require('../utils/jwt-utils');

module.exports.getAuth = async (req, res, next) => {

  // retrieving user authorization token from request
  const token = req.headers["authorization"] ? req.headers["authorization"].replace('Bearer ', '') : 'null';

  // check if token exists
  if(token !== 'null'){
    // decode user token
    const decodedToken = verifyToken(token);

    // setting current user info in request
    req.currentUser = decodedToken;
  }

  next();

};
