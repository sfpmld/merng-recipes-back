const jwt = require('jsonwebtoken');

const JWT_SALT = process.env.JWT_SALT;
const JWT_TOKEN_EXPIRATION = process.env.JWT_TOKEN_EXPIRATION || '3h';

module.exports = {

  generateToken: (data) => {
    return jwt.sign(
        data,
        JWT_SALT,
        { expiresIn: `${JWT_TOKEN_EXPIRATION}` });
  },

  verifyToken: (token) => {
    let decodedToken;

    try {
      decodedToken = jwt.verify(token, JWT_SALT)
    } catch (err) {
      err.statusCode = 500;
      throw err;
    }

    return decodedToken;
  }

}
