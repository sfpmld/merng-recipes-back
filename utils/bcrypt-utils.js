const bcrypt = require('bcrypt');

module.exports = {
  hashPassword: (password) => {
    const hashPass = bcrypt.hash(password, 12);
    return hashPass;
  },

  verifyPassword: (password_1, password_2) => {
    const areSame = bcrypt.compare(password_1, password_2);
    return areSame;
  }
}
