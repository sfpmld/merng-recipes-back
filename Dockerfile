# Dockerfile for nodeJS and reactJS container web application project

## Base nodeJS install
FROM node:10.16.0 AS base_node
LABEL maintainer="Xavier ABENAQUI <xavier.abenaqui@gmail.com>"

#installation of utils
# Fix for bug {Failed to fetch http://deb.debian.org/debian/dists/jessie-updates/InRelease  Unable to find expected entry 'main/binary-amd64/Packag es' in Release file (Wrong sources.list entry or malformed file)", "", "E: Some index files failed to download. They have been ignored, or old ones used instead} -->> cf https://superuser.com/questions/1423486/issue-with-fetching-http-deb-debian-org-debian-dists-jessie-updates-inrelease.
# RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list

#The following packages have unmet dependencies:", " apt-utils : Depends: apt but it is not going to be installed
#https://forum.level1techs.com/t/can-i-go-ahead-and-downgrade-apt-to-upgrade-apt-utils-ubuntu-server-16-04/118116
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y apt-utils \
               curl \
               iputils-ping \
               expect \
    && rm -rf /var/lib/apt/lists/*

## Dependencies
FROM base_node AS node_dependencies

# creation of workdir
WORKDIR  /back

COPY ./package.json /back/

RUN npm install --save \
# hot reloading in dev environment
    && npm install -g nodemon \
# GraphQL
    && npm install -g env-cmd

## Copy of project source
FROM node_dependencies AS node_src

WORKDIR  /back

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /back/node_modules/.bin:$PATH

# copy of backend project source
COPY ./ /back



# we have to bypass the command npm start to reduces the number of processes running inside of your container. Secondly it causes exit signals such as SIGTERM and SIGINT to be received by the Node.js process instead of npm swallowing them
# https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md#cmd
CMD npm run debug
