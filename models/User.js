const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const { hashPassword } = require('../utils/bcrypt-utils');

const userSchema = new Schema({
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  favorites: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Recipe',
    }
  ],
}, { timestamps: true });

userSchema.pre('save', async function(next) {
  if(!this.isModified('password')){
    return next();
  }
  const hashPass = await hashPassword(this.password)
  this.password = hashPass;
  next();
});



module.exports = mongoose.model('User',userSchema);
