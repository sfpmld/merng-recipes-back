const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const recipeSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  likes: {
    type: Number,
    default: 0
  },
  instructions: {
    type: String,
    required: true
  },
  username: {
    type: String
  }
}, { timestamps: true });

recipeSchema.index({
  'name': 'text',
  'instructions': 'text',
  'description': 'text'
  },
  { weights: {
      name: 5, instructions:2, description: 3
    }
  }
);


module.exports = mongoose.model('Recipe',recipeSchema);
